aws lambda add-permission \
	--function-name google_trends_webhook \
	--statement-id 1 \
	--action 'lambda:InvokeFunction' \
	--principal events.amazonaws.com \
	--source-arn arn:aws:events:eu-west-2:278348348837:rule/fire-once-a-day
