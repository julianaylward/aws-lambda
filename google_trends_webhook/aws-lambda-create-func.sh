#Run this file to create and load the sparrow2 lambda function to AWS
#Role asigns privilidges, sparrow role has access to paramstore and basic lambda which is all we need
aws lambda create-function \
	--function-name google_trends_webhook \
	--runtime python3.8 \
	--role arn:aws:iam::278348348837:role/service-role/sparrow-role \
	--handler google_trends.handler \
	--zip-file fileb://./package.zip
