#Import libs
from pytrends.request import TrendReq
import requests
from load_aws_ssm_secrets import get_secret #this references the function we've built rather than a python library!

#Get Webhook URL from AWS SSM Secrets
webhook_url = get_secret("RI_TEAMS_CA_WEBHOOK")

#Create request object
def get_trends():
    print("Trends launching")
    pytrend = TrendReq()
    trends = pytrend.trending_searches(pn='united_kingdom')
    #Sending simple text message in json format:
    #Convert DF to dict
    trends_dict = trends[0].to_dict()
    #Convert to text string to send as plain text JSON
    out = "Today's top UK Trends on Google are: "
    for i in trends_dict:
        out = out + trends_dict[i] + ", "
    message = {"text" : out}
    print("Trends fetched")
    return message

#Create post request to webhook
def post_to_webhook(message):
    #Create a post request to send the message, formatting the data as json
    x = requests.post(webhook_url, json = message)
    if x.text == '1': 
        print ("success")
    else:
        print("failed")
    print (x.text)

#This is the function we'll instruct lambda to invoke
def handler(event, context):
    print("Handler starting...")
    #Call Google Trends API
    message = get_trends()
    #Call json post request
    post_to_webhook(message)
    print("Function completed successfully")






