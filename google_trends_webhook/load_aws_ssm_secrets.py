#import libs

import boto3
import os
#from dotenv import find_dotenv, load_dotenv

#dotenv_path = find_dotenv()
#load_dotenv(dotenv_path)

#Get parameters stored in parameter store by param name

def get_secret(parameter_name):
    """Get a parameter from SSM Parameter store and decrypt it"""
    ssm = boto3.client('ssm')
    parameter = ssm.get_parameter(
        Name=parameter_name,
        WithDecryption=True
    )['Parameter']['Value']
    return parameter

#Put parameters into the param store as encrypted secure strings

def put_secret(parameter_name, parameter_value):
    """Put a parameter inside SSM Parameter store with encryption"""
    print('Putting a parameter with name of ' + parameter_name + ' into SSM.')
    ssm = boto3.client('ssm')
    ssm.put_parameter(
        Name=parameter_name,
        Value=parameter_value,
        Type='SecureString',
        Overwrite=True
    )
    print("Successfully added a parameter with the name of: " + parameter_name)

#Uncomment to load secrets initially and to test locally
# Example of using put_secret() to add your keys
#SECRETS = {
#     "RI_TEAMS_CA_WEBHOOK": os.environ["RI_TEAMS_CA_WEBHOOK"]
# }

#for parameter_name, parameter_value in SECRETS.items():
#     put_secret(parameter_name, parameter_value)

#print(SECRETS)

#Test get_secret func
#print(get_secret("RI_TEAMS_CA_WEBHOOK"))

