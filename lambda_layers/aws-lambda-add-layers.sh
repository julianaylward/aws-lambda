aws lambda publish-layer-version \
--layer-name pandas-ga-api \
--description "Query Google Analytics reporting API. Import Data to Pandas" \
--license-info "MIT" \
--zip-file fileb://layer.zip \
--compatible-runtimes python3.6 python3.7 python3.8