Following Pluralsight AWS Lambda demo, set up a twitter bot running on lambda with python \
Store creds in parameter store and create deployment package for Twython library dependency \
Credentials stored locally in .env file \
Ensure you've correctly set up the AWS CLI by running  `aws configure` prior to any scripts in this repo \
- Run `bash setup.sh` to generate the zipped deployment package with scripts and dependencies for upload to lambda \
- To create and load your lambda function run `bash aws-lambda-create-func.sh`
- Create a cloudwatch event trigger by running `bash aws-cloudwatch-create-schedule.sh`
- To view rules you've created run `aws events list-rules`
- Assign the cloudwatch event to the lambda function by runing `aws-lambda-add-permission.sh`
`


