#Run this file to create and load the sparrow2 lambda function to AWS
aws lambda create-function \
	--function-name sparrow \
	--runtime python3.8 \
	--role arn:aws:iam::278348348837:role/service-role/sparrow-role \
	--handler sparrow.handler \
	--zip-file fileb://./package.zip
