#This script compeltes all the necessary steps required to create the deployment package for the twitter bot
#By creating a virtual env, loading python library dependencies, removing the venv and zipping up the package
#!/bin/bash


# Create and initialize a Python Virtual Environment
echo "Creating virtual env - .venv"
python3.8 -m venv .venv

echo "sourcing virtual env - .venv"
source .venv/bin/activate

# Create a directory to put things in
echo "Creating 'setup' directory"
mkdir setup

# Move the relevant files into setup directory
echo "Moving function file(s) to setup dir"
cp sparrow.py setup/
cp load_aws_ssm_secrets.py setup/
cd ./setup

# Install requirements in target (-t .) directory
echo "pip installing requirements from requirements file in target directory"
pip install -r ../requirements.txt -t .

# Prepares the deployment package 
#zip -r <location to output> <files/dir to zip>
#in this case, we need the contents of the dir, but not the setup dir itself, hence ./
echo "Zipping package"
zip -r ../package.zip ./*

# Remove the setup directory used
echo "Removing setup directory and virtual environment"
cd ..
rm -rf ./setup
deactivate
rm -rf ./.venv
# changing dirs back to dir from before
echo "Opening folder containg function package - 'package.zip'"
#open .
